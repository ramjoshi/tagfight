#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp import template

import logging
import os
from collections import defaultdict
import urllib2

from BeautifulSoup import BeautifulSoup
import simplejson
from stemming.porter2 import stem

class MainHandler(webapp.RequestHandler):
    def get(self):
		path = os.path.join(os.path.dirname(__file__), 'index.html')
		template_values = {}
		self.response.out.write(template.render(path, template_values))

class StemHandler(webapp.RequestHandler):
	def post(self):
		tags = self.request.get("tags").replace(" ", "").split(",")
		counts = map(int, self.request.get("counts").replace("", "").split(","))
		tagsCounts = zip(tags, counts)
		stemmedTags = map(stem, map(unicode.lower, tags))
		seen = {}
		i = 0
		for stemTag in stemmedTags:
			if stemTag in seen.keys():
				index = stemmedTags.index(stemTag) 
				tag = tagsCounts[index][0]
				newCount = tagsCounts[index][1] + tagsCounts[i][1]
				tagsCounts[index] = (tag, newCount)
				stemmedTags[i] = ''
				tagsCounts[i] = ''
			else:
				seen[stemTag] = 1
			i += 1
		results = []
		for elem in tagsCounts:
			if elem != '':
				results.append(elem)			
		self.response.out.write(simplejson.dumps(results))

class TermFreqHandler(webapp.RequestHandler):
	def post(self):
		terms = self.request.get("terms").split(",")
		url = self.request.get("url")
		page = urllib2.urlopen(url)
		words = ''.join(BeautifulSoup(page.read()).findAll(text=True))
		# d = word_count(words)
		d = defaultdict(int)
		for word in words.split():
			d[word] += 1
		results = {}
		for term in terms:
			freq = 0
			if term in d.keys():
				freq = d[term]
			results[term] = freq
		self.response.out.write(simplejson.dumps(results))
		
def main():
    application = webapp.WSGIApplication([('/', MainHandler), 
									('/stem', StemHandler),
									('/termFreq', TermFreqHandler)],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()