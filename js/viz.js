function drawDeliciousViz(sortedTags, tags, termFreq) {
	// Create and populate the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'x');
	data.addColumn('number', '# of users');
	data.addColumn('number', 'term frequency');
	
	$.each(sortedTags, function(index, value) {
		data.addRow([value, tags[value], termFreq[value]]);
	});

	// Create and draw the visualization.
	new google.visualization.LineChart(document.getElementById('deliciousViz')).
	draw(data, {curveType: "function",
	colors: ['#1462C1', '#FC575E'], 
	backgroundColor: '#E7E7DE',
	width: 1000, height: 500,
	vAxis: {maxValue: tags[sortedTags[0]]},
	//hAxis: {slantedTextAngle: 90},
	//fontSize: 10,
	legend: "top",
	title: "Delicious Tags distribution with long tail effect"}
);
}

function drawStemmedViz(sortedStemmedTags, stemmedTags, termFreq) {
	// Create and populate the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'x');
	data.addColumn('number', '# of users');
	data.addColumn('number', 'term frequency');

	$.each(sortedStemmedTags, function(index, value) {
		data.addRow([value, stemmedTags[value], termFreq[value]]);
	});

	// Create and draw the visualization.
	new google.visualization.LineChart(document.getElementById('stemmedViz')).
	draw(data, {curveType: "function",
	colors: ['#666', '#FC575E'],
	backgroundColor: '#E7E7DE',
	width: 1000, height: 500,
	vAxis: {maxValue: stemmedTags[sortedStemmedTags[0]]},
	//hAxis: {slantedTextAngle: 90},
	legend: "top",
	title: "Stemmed Tags distribution with long tail effect"}
);
}

// Combining Both Tags and Stemmed Tags on the Same Chart

function drawcombinedViz(sortedTags, tags) {
	// Create and populate the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'x');
	data.addColumn('number', '# of users -delicious');
	data.addColumn('number', '# of users -stemmed');
	
	$.each(sortedTags, function(index, value) {
		//data.addRow([value, tags[value], stemmedTags[value]]);
		data.addRow([value, stemmedTags[value]]);
	});

	// Create and draw the visualization.
	new google.visualization.LineChart(document.getElementById('combinedViz')).
	draw(data, {curveType: "function",
	colors: ['#1462C1', '#666'],
	backgroundColor: '#E7E7DE',
	width: 1000, height: 500,
	vAxis: {maxValue: tags[sortedTags[0]]},
	hAxis: {slantedTextAngle: 90},
	//fontSize: 10,
	legend: "top",
	title: "Delicious Tags distribution with long tail effect"}
);
}

/*
function drawCombinedViz() {
	// Create and populate the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'x');
	data.addColumn('string', 'x');
	data.addColumn('number', '# of users');
	data.addColumn('number', '# of users');

	$.each(sortedStemmedTags, sortedTags, function(index, value) {
		data.addRow([value, stemmedTags[value], tags[value]]);
	});

	// Create and draw the visualization.
	new google.visualization.LineChart(document.getElementById('stemmedViz')).
	draw(data, {curveType: "function",
	colors: ['#666'],
	backgroundColor: '#E7E7DE',
	width: 1000, height: 500,
	vAxis: {maxValue: stemmedTags[sortedStemmedTags[0]]},
	hAxis: {slantedTextAngle: 90},
	legend: "top",
	title: "Stemmed Tags distribution with long tail effect"}
);
}
*/