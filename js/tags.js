$(document).ready(function() {
	if(!window.console){ window.console = {log:function(){}}; }
	//### Global variables ###
	var url = ""; // The url being analysed
	var tags = {}; // Delicious tag:value pairs from Delicious bookmarks
	var sortedTags = []; // Sorted tags
	var termFreq = {}; // Term frequency
	var stemmedTags = {}; // Stemmed tag:value pairs from Porter2
	var sortedStemmedTags = []; // Sorted stemmed tags 
	var entities = new Array(); // An array for holding machine tags

	//### Form bindings ###

	$('#url-form').submit(function(){
		url = $('#url-input').val();

		//Delicious is very picky with the format of URLs
		//add http:// if not present
		if(url.search(/^http:\/\//)==-1)
		url = 'http:\/\/' + url;
		//check if page ends in .html
		if(url.search(/html$/)==-1) {
			//if not, add a final backslash if one isn't present
			if(url.search(/\/$/)==-1) {
				url = url + "\/"
			}
		}

		getDeliciousTags(url);
		getEvriEntities(url);
		return false; 
	});

	// Get tags from Delicious
	function getDeliciousTags(siteUrl) {
		var postData = {
			count: 100
		};

		$.getJSON("http://feeds.delicious.com/v2/json/url/" + $.md5(siteUrl) + "?callback=?", postData, function(rsp) {
			//console.log("### Delicious tags for 100 bookmarks for the url");

			keyArray = []; // Initiliaze an array to store just the tags

			// Loop over all results and save tags to object, incrementing
			// their count if a tag already existed
			$.each(rsp, function(index, object) {
				$.each(object.t, function(index, value) {
					var tag = value.replace(",","").replace(" ","");

					if (typeof(tags[tag]) !='undefined') {
						tags[tag] = tags[tag] + 1
					} else {
						tags[tag] = 1;
						keyArray.push(tag);
					}
				});
			});
			sortedTags = sortByValue(keyArray, tags).reverse();
			$("#list").html("");
			$("#humanTags h3").html("Top <span class='tag'>Delicious</span> tags (from 100 users)");
			$.each(sortedTags, function(index, value) {
				//console.log("( " + value + " ) - " + tags[value] + ' users');
				$("#list").append("<li><span class='tag'>" + value + "</span> - " + tags[value] + " users</li>");
			});

			// Get term frequence of tags
			postData = {
				terms: sortedTags.join(","),
				url: siteUrl
			};
			$.post("/termFreq",
			postData, function(tf) {
				//console.log("### Tags Term frequency ###");
				//console.log(tf);
				termFreq = tf;

				// Perform stemming analysis of Delicious tags
				getStemmedTags(sortedTags, tags);

				plotDeliciousTags(sortedTags, tags, tf);
			}, "json");
		});
		} // end of getDeliciousTags function

		//code adapted from http://bytes.com/topic/javascript/answers/153019-sorting-associative-array-keys-based-values
		function sortByValue(keyArray, valueMap) {
			return keyArray.sort(function(a,b){return valueMap[a]-valueMap[b];});
		}

		//### Stemming ###

		function getStemmedTags(sortedTags, tags) {
			counts = [];
			$.each(sortedTags, function(index, value) {
				counts.push(tags[value]);
			});
			postData = {
				tags: sortedTags.join(","),
				counts: counts.join(",")
			}
			$.post("/stem",
			postData, function(rsp) {
				//console.log(rsp);
				keyArray = []; // Initiliaze an array to store just the tags
				$.each(rsp, function(index, value) {
					keyArray.push(value[0]);
					stemmedTags[value[0]] = value[1];
				});
				sortedStemmedTags = sortByValue(keyArray, stemmedTags).reverse();
				$("#stems").html("");
				$("#stemmedTags h3").html("Top <span class='tag stem-tag'>Stemmed</span> tags (from Porter2 algorithm)");
				$.each(sortedStemmedTags, function(index, value) {
					//console.log(value);
					$("#stems").append("<li><span class='tag stem-tag'>" + value + "</span> - " + stemmedTags[value] + " users</li>");
				});

				plotStemmedTags(sortedStemmedTags, stemmedTags, termFreq);
			}, "json");

		}

		//### Functions for calling APIs to retrieve tags and entities ###

		// Get entities from Evri
		function getEvriEntities(siteUrl) {
			var postData = {
				uri: siteUrl
			};

			$.getJSON("http://api.evri.com/v1/media/entities.json?callback=?",
			postData, function(rsp){
				$("#entities").html("");
				$("#machineTags h3").html("Top Machine tags (from <span class='tag evri-tag'>Evri</span> and/or <span class='tag alchemy-tag'>Alchemy</span> entity extraction)");
				//console.log("### Evri entities");
				if (typeof(rsp.evriThing.graph.entities.entity) !='undefined'){
					$.each(rsp.evriThing.graph.entities.entity, function(index, value){
						console.log(value.name.$);
						$("#entities").append("<li><span class='tag evri-tag'>" + value.name.$ + "</span></li>");
						entities.push(value.name.$);
					});
					// If Evri returns less than 10 entities, get more entities from Alchemy
					if(rsp.evriThing.graph.entities.entity.length < 10) {
						getAlchemyEntities(siteUrl)
					}
				} else {
					//console.log("No Evri entities found");
					getAlchemyEntities(siteUrl);
				}
				console.log("### Evri categories")
				if (typeof(rsp.evriThing.graph.categories.category) !='undefined') {
					if($.isArray(rsp.evriThing.graph.categories.category)) {
						$.each(rsp.evriThing.graph.categories.category, function(index, value){
							//console.log(value.$);
						});
					} else {
						//console.log(rsp.evriThing.graph.categories.category.$);
					}
				} else {
					//console.log("No categories found");
				}
			});

			} // end of getEvriEntities function

			// Get entities from Alchemy
			function getAlchemyEntities(siteUrl) {		
				var postData = {
					url: siteUrl,
					apikey: "c65e25c5dbef22608a1fe6591474e4ea02ab4a7b",
					outputMode: "json"
				};

				$.getJSON("http://access.alchemyapi.com/calls/url/URLGetRankedNamedEntities?jsonp=?",
				postData, function(rsp){
					//console.log("### Alchemy entities")
					if (typeof(rsp.entities) !='undefined') {
						if($.isArray(rsp.entities)) {
							$.each(rsp.entities, function(index, value){
								//console.log(value.text + " ("+value.count+")");
								$('#entities').append("<li><span class='tag alchemy-tag'>" + value.text + "</span></li>");
								entities.push(value.text);
							})
						} else {
							//
						}
					} else {
						//console.log("No Alchemy categories found");
					}
				});
				} // end of getAlchemyEntities function

			});